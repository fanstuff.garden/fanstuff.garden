module.exports = {
	title: "Fanstuff Garden",
	url: "https://fanstuff.garden/",
	language: "fr",
	description: "Un ensemble de petit fansites",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
