---
layout: layouts/base.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

# <img src="/img/drgn_nom_book_256.png" alt="" height="32" width="32"> {{ metadata.title }}

Bienvenue sur {{ metadata.title }} !

Ce site est un ensemble de fansites oldschool à l'ancienne, inspiré un peu des "shrine" (ces pages dédiées à un sujet très spécifiques) de jadis. La plupars sont écrites sous forme de sites statiques générés à partir de fichier texte. Le but de ce site est d'être l'endroit où je pose tout mes brainrot sur des séries et fandoms que j'aime.

Ce(s) site(s) est hébergé par [kobold.cafe](https://kobold.cafe) et géré par [kazhnuz](https://kazhnuz.space). Leur sources sont disponibles dans ce [groupe git](https://git.kobold.cafe/fanstuff.garden)

## <img src="/img/drgn_wrench_256.png" alt="" height="24" width="24"> Liste des sites

Voici la liste des sites actuellement hébegés par fanstuff.garden :

- [Sonic Garden](https://sonic.fanstuff.garden) - Un fansite/shrine Sonic the Hedgehog.
- [Press Garden](https://press.fanstuff.garden) - Un site de news SEGA et Sonic the Hedgehog.
- [Missing Number](https://missing-number.fanstuff.garden) - Un fansite Pokémon sur la 1G/2G

## <img src="/img/drgn_think_confused_256.png" alt="" height="24" width="24"> Pourquoi ces sites ?

Une grosse partie des informations et du discours sur les séries est concentré en quelques gros sites qui se sont beaucoup professionnalisé (et certains sont présent sur des plateforme devenant de moins en moins saines, tel que Fandom). Il est important d'avoir des plus petits sites, qui se concentre sur des sujets un peu plus "niches", et/ou permettent à des discours différents, plus amateurs d'être visible.

S'il en existe déjà (par exemple sur des plateformes comme neocities), une bonne partie de ces plus petits sites sont anglophones, ce qui leur rend un accès plus difficile pour les personnes ne parlant pas anglais. Le but de Fanstuff Garden est du coup de créer des petits sites francophones parlant de trucs funs, et un peu plus "libre".

## <img src="/img/drgn_science_256.png" alt="" height="24" width="24"> Articles parlant de fandoms

Voici quelques articles que j'ai écris sur mes différents blogs qui parlent de sujets fandoms et assimilés :

- 2024-03-20 - [Construire de meilleurs fandoms](https://blog.kazhnuz.space/construire-de-meilleurs-fandoms)
- 2024-03-13 - [Les soucis dans les fandoms](https://blog.kazhnuz.space/construire-de-meilleurs-fandoms)
- 2023-09-10 - [Il faut accorder moins d’importance au « canon »](https://blog.kazhnuz.space/il-faut-accorder-moins-dimportance-au-canon/)
- 2019-01-29 - [Fanfictions](https://blog.kazhnuz.space/fanfictions/)

## <img src="/img/drgn_wrench_256.png" alt="" height="24" width="24"> A venir

Les projets en cours/futurs sur fanstuff garden et autour

- Un site retrogaming
- Un site sur l'environnement de bureau GNOME
- Un site présentant des livres cool de fantasy

### <img src="/img/drgn_lurk_owo_256.png" alt="" height="24" width="24"> Projet potentiels futurs

Des projets plus "nice to have", sans vraiment des plans de définis.

- Un (nouveau) site d'élevage de Chao
- Un site Pokémon sur la 3G/4G/5G
- Des espace communautaires fandom queer/neurodivergent-friendly
  - Un/des forums ?
  - Un/des canaux XMPP/matrix/IRC ?

## <img src="/img/drgn_nom_cat_heart_256.png" alt="" height="24" width="24"> Crédits

- Fond utilisant des Chipset/Tileset [EasyRPG](https://github.com/EasyRPG/RTP) par Jason sous licence CC-BY 4.0
- Emoji [drgns v3.0](https://volpeon.ink/emojis/drgn/) créés par [Volpeon](https://volpeon.ink/) sous licence CC-BY-NC-SA 4.0